import { NextFunction, Request, Response } from "express";

function logger(request: Request, response: Response, next: NextFunction) {
  console.log(`caught request: ${request.url}`)
  next();
}

export default logger