import { ICredentials } from "@app/dto/credentials";

export interface IAuthService {
  credentialsAreValid(credentials: ICredentials): Promise<boolean>;
}
