import AuthServiceImpl from "@app/service/auth/AuthServiceImpl";
import { IAuthService } from "@app/service/auth/IAuthService";

export * from "./IAuthService";
export * from "./AuthServiceImpl";

export default {
  makeAuthService: () => new AuthServiceImpl() as IAuthService,
};
