import { IAuthService } from "./IAuthService";
import { ICredentials } from "@app/dto/credentials";
import { connect } from "@app/service/db";
import bcrypt from "bcrypt";

class AuthServiceImpl implements IAuthService {
  public async credentialsAreValid(credentials: ICredentials) {
    try {
      const client = await connect();
      const database = client.db("oauth_db");
      const collection = database.collection("users");
      const user = await collection.findOne({
        username: credentials.username,
      });

      if (!user) {
        throw new Error("user is not found");
      }

      const isPasswordMatches = await bcrypt.compare(
        credentials.password,
        user.password
      );

      console.log(`[${AuthServiceImpl.name}]`, "retrieved user: ");
      console.dir(user);

      return isPasswordMatches;
    } catch (e) {
      console.error("AuthServiceImpl", e);
      return false;
    }
  }
}

export default AuthServiceImpl;
