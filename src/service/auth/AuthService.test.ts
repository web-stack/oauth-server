import auth from "@app/service/auth";

describe("AuthService", function () {
  const authService = auth.makeAuthService();

  it("should validate given credentials", function () {
    const result = authService.credentialsAreValid({
      username: "smith",
      password: "smith123",
    });

    expect(result).toBeTruthy();
  });
});
