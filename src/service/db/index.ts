import { MongoClient } from "mongodb";
const db = require("../../../config/db");

export async function connect(): Promise<MongoClient> {
  try {
    return await MongoClient.connect(db.url, { useUnifiedTopology: true });
  } catch (e) {
    throw e;
  }
}
