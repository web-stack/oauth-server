import auth from "./auth";
import token from "./token";

export const authService = auth.makeAuthService();
export const tokenService = token.makeTokenService();
