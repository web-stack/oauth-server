import { ICredentials } from "@app/dto/credentials";

export interface ITokenService {
  generateToken(credentials: ICredentials): string;

  isTokenValid(token: string): Promise<boolean>;
}
