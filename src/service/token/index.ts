import TokenServiceImpl from "@app/service/token/TokenServiceImpl";
import { ITokenService } from "@app/service/token/ITokenService";

export * from "./ITokenService";

export default {
  makeTokenService: () => new TokenServiceImpl() as ITokenService,
};
