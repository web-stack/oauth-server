import { ITokenService } from "@app/service/token/ITokenService";
import jwt from "jsonwebtoken";
import tokenConfig from "@config/token";
import { ICredentials } from "@app/dto/credentials";
import { authService } from "@app/service";

class TokenServiceImpl implements ITokenService {
  generateToken(credentials: ICredentials): string {
    return jwt.sign(credentials, tokenConfig.secret, {
      expiresIn: Math.floor(Date.now() / 1000) + 60 * 60,
    });
  }

  isTokenValid(token: string): Promise<boolean> {
    try {
      const credentials = jwt.verify(token, tokenConfig.secret) as ICredentials;
      return authService.credentialsAreValid(credentials);
    } catch (e) {
      console.error(TokenServiceImpl.name, e);
      return Promise.resolve(false);
    }
  }
}

export default TokenServiceImpl;
