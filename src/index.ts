import bodyParser from "body-parser";
import express from "express";
import cors from "cors";
import router from "@app/router/index";
import logger from "@app/middleware/logger";

const app = express();

app.use(cors());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(bodyParser.json());
app.use(logger);

app.get("/", (_, res) => res.json({ message: "hello there" }));
app.use(router);

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log("[APP]", `running on port ${port}`);
});
