import OAuthTokenHandler from "./OAuthToken.handler";
import express from "express";

const router = express.Router();

router.post("/oauth/token", OAuthTokenHandler.generateTokenHandler);
router.post("/oauth/authorize", OAuthTokenHandler.authorizeHandler);

export default router;
