import { Request, Response } from "express";
import { authService, tokenService } from "@app/service";
import { ICredentials } from "@app/dto/credentials";

async function generateTokenHandler(req: Request, res: Response) {
  if (!req.body.username || !req.body.password) {
    return res.json({ code: "error", message: "credentials aren't valid" });
  }

  const credentials: ICredentials = {
    username: req.body.username,
    password: req.body.password,
  };

  if (!(await authService.credentialsAreValid(credentials))) {
    return res.json({ code: "error", message: "credentials aren't valid" });
  }

  return res.json({
    code: "ok",
    access_token: tokenService.generateToken(credentials),
  });
}

async function authorizeHandler(req: Request, res: Response) {
  try {
    const tokenHeader = req.header("Authorization") || "";
    const token = tokenHeader.split("Bearer ")[1];

    const isTokenValid = await tokenService.isTokenValid(token);
    if (!isTokenValid) {
      throw new Error("error in token validation");
    }

    return res.json({
      code: "ok",
    });
  } catch (e) {
    return res.json({
      code: "error",
      message: "token is not valid",
      error: e,
    });
  }
}

export default {
  generateTokenHandler,
  authorizeHandler,
};
