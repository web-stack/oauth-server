const yargs = require("yargs");
const mongoClient = require("mongodb").MongoClient;
const config = require("../config/db");
const bcrypt = require("bcrypt");

async function run({ username, password }) {
  const client = await mongoClient.connect(config.url, {
    useUnifiedTopology: true,
  });
  const database = client.db(config.name);
  const collection = database.collection("users");

  return await collection.insertOne({
    username: username,
    password: await bcrypt.hash(password, config.salt),
  });
}

try {
  console.log("Adding user");
  const argv = yargs
    .command("add", "Creates a new user", {
      username: {
        description: "username",
        type: "string",
        alias: "u",
      },
      password: {
        description: "password",
        type: "string",
        alias: "p",
      },
    })
    .help()
    .alias("help", "h").argv;

  if (argv._.includes("add")) {
    const username = argv.username;
    const password = argv.password;

    if (!username) throw new Error("username is invalid");
    if (!password) throw new Error("username is invalid");

    run({ username, password }).then((res) => {
      console.log("Done");
      console.log(res);
      process.exit();
    });
  }
} catch (e) {
  console.log(e);
  process.exit(1);
}
