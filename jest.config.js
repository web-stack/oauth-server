module.exports = {
  rootDir: "./src",
  preset: "ts-jest",
  testEnvironment: "node",
  moduleNameMapper: {
    "@app/service/(.*)": "<rootDir>/service/$1",
  },
};
