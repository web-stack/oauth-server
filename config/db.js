const MONGO_DB_URL = process.env.MONGO_DB_URL || "0.0.0.0";

console.log(MONGO_DB_URL);

module.exports = {
  url: `mongodb://${MONGO_DB_URL}:27017/`,
  name: "oauth_db",
  salt: 12,
};
